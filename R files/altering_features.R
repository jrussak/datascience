#set path to my directory
#setwd("C://Users//Getter//Documents//autolog//denmark_smalls")

#load dataframe with all the objects that have both types of trips
#load("dfBothTypeTrips.RData")
df <- dfBothTypeTrips
str(df)

#change data type of ID-s from numeric to factor
df$tripid <- factor(df$tripid)
df$orgid <- factor(df$orgid)
df$objectid <- factor(df$objectid)
df$driverid <- factor(df$driverid)
df$startlocationid <- factor(df$startlocationid)
df$endlocationid <- factor(df$endlocationid)

#handling time
#split timestamps into date and time
#install.packages("data.table")
library(data.table)
a <- as.character(df$starttimestamp)
parts <- t(as.data.table(strsplit(a,' ')))
df$startDate <- parts[,1]
df$startTime <- parts[,2]
b <- as.character(df$endtimestamp)
parts2 <- t(as.data.table(strsplit(b,' ')))
df$endTime <- parts2[,2]
#endDate in most cases is the same as startDate so will be left out

#get month and weekday from date
dates <- as.POSIXct(as.character(df$startDate),format = "%d/%m/%Y")
df$month <- months(dates)
df$weekDay <- weekdays(dates)
df$month <- factor(df$month)
df$weekDay <- factor(df$weekDay)

#convert time to seconds from midnight to handle it numerically in models
#might not be the best solution as day is cyclic and so numerically two furthest points
#in real life are actually the closest ones to each other (e.g. 0 and 86399 basically 
#are the same time)
midnight <- as.POSIXct("00:00:00", format = "%H:%M:%S")
a <- as.POSIXct(as.character(df$startTime),format = "%H:%M:%S")
b <- as.POSIXct(as.character(df$endTime),format = "%H:%M:%S")
diffa <- difftime(a,midnight,units="secs")
diffb <- difftime(b,midnight,units="secs")

df$startTimeMidnight <- as.numeric(diffa)
df$endTimeMidnight <- as.numeric(diffb)

#create features for first and last trip of the day
df$firstTrip <- 0
df$lastTrip <- 0
df$firstTrip[df$nth_asc == 1] <- 1
df$lastTrip[df$nth_desc == 1] <- 1
df$firstTrip <- factor(df$firstTrip)
df$lastTrip <- factor(df$lastTrip)

#create feature to see how long the stop after the trip was

prevTripEnd <- as.character(df$endtimestamp)
prevTripEnd <- as.POSIXct(prevTripEnd,format="%d/%m/%Y %H:%M:%S")

nextTripStart <- as.character(df$starttimestamp)
nextTripStart <- nextTripStart[-1]
nextTripStart <- c(nextTripStart,NA)
nextTripStart <- as.POSIXct(nextTripStart,format="%d/%m/%Y %H:%M:%S")

diff <- difftime(prevTripEnd,nextTripStart)

df$stoppedAfter <- diff
df$stoppedAfter <- -as.numeric(df$stoppedAfter)

un <- unique(df$objectid)

#removing each object's last trip as we do not know the stopping time after that trip
i <- 0
for(el in un){
  print(i/length(un))
  df <- df[-max(which(df$objectid == el)),]
  i <- i+1
}

#small histograms to show the distribution of the trip times and stoptimes
hist(prevTripEnd,breaks = 'weeks')
hist(df$stoppedAfter)

#just in case saving the current version 

#save(df,file='dfBothTypeTrips_middleversion.RData')
load('dfBothTypeTrips_middleversion.RData')
